package com.example.android.unscramble.ui.game

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel

// ViewModel ส่วนของข้อมูล
class GameViewModel : ViewModel(){


    override fun onCleared() {
        super.onCleared()
        Log.d("GameFragment","ViewModel destroy!!!")
    }

    private var _score = MutableLiveData<Int>(0)
    private var _currentWordCount = MutableLiveData<Int>(0)
    private  var _currentScrambledWord =  MutableLiveData<String>()

    //back property
    val score : LiveData<Int> get() = _score
    val currentWordCount : LiveData<Int> get()= _currentWordCount
    val currentScrambledWord : LiveData<String> get()= _currentScrambledWord

    //transformation map
    val currentScambleWordWithBracket = Transformations.map(currentScrambledWord) {
          "(${_currentScrambledWord.value})"
    }


    //words
    private  var wordList : MutableList<String>  =  mutableListOf()
    private lateinit var currentWord :String

    //init ขณะที่มีการ render Appขึ้นมาครั้งแรก
    init{
        Log.d("GameFragment","ViewModel Created!!")
        getNextWord()
    }

    private fun getNextWord(){
        currentWord = allWordsList.random() //allWordsList เอามาจาก import
        val tempWord = currentWord.toCharArray()
        tempWord.shuffle()

        //เช็คจนกว่าจะ shuffle จริงๆ
        while(String(tempWord).equals(currentWord,false)){
            tempWord.shuffle()
        }

        if(wordList.contains(currentWord)){
            // กรณีที่ wordList มีี currentWord แล้ว
            getNextWord()
        }else{
            //กรณี wordList ยังไม่มี currentWord
            _currentScrambledWord.value= String(tempWord)
            _currentWordCount.value = _currentWordCount.value?.plus(1)
            wordList.add(currentWord)
        }

    }

    //check ว่าครบข้อหรือยัง
    fun nextWord(): Boolean{
        return if((_currentWordCount.value)!!< MAX_NO_OF_WORDS){
            //ทำต่อไป
            getNextWord()
            return true
        }else {
            return false
        }
    }

    //เพิ่มคะแนน
    private fun increaseScore(){
        _score.value = _score.value?.plus(SCORE_INCREASE)
    }

    fun isUserWordCorrect(playerWord: String): Boolean{
        if(playerWord.equals(currentWord,true)){
            increaseScore()
            return true
        }
        return false
    }


    fun reintializeData(){
        _score.value=0
        _currentWordCount.value=0
        wordList.clear()
        getNextWord()
    }

}